﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class currentGameSituation : MonoBehaviour {
    public static bool gameOver = false;
    public static bool inGame = false;
    public static bool transition = false;
    public static bool firstLaunch = true;
    public static int currentCoins = 0;
    public static int bestCoins = 0;
    public static int totalCoins = 0;
    public Text currentCoinsText;
    public Text finishCoinsText;
    public Text totalCoinsText;
    public Text bestCoinsText;
    void FixedUpdate() {
        if(inGame == true) {
            currentCoinsText.text = currentCoins.ToString();
            finishCoinsText.text = currentCoins.ToString();
            bestCoinsText.text = bestCoins.ToString();
        } else if(inGame == false) {
            totalCoinsText.text = totalCoins.ToString();
        }
    }
    public static void loadSceneNumb(int sceneNumb) {
        if(sceneNumb == 0) {
            currentGameSituation.inGame = false;

            SceneManager.LoadScene(0);
        } else if(sceneNumb == 1) {
            transition = false;
            gameOver = false;
            inGame = true;

            carController.currentSpeed = 2.0f;

            SceneManager.LoadScene(1);

            currentCoins = 0;
        }
        
        Time.timeScale = 1;
    }
}
