﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class treeGenerator : MonoBehaviour {
    public GameObject tree;
    private int left_right;
    void Start() {
        //InvokeRepeating("treeSpawn", Random.Range(10, 30), Random.Range(10, 20));
        StartCoroutine(treeSpawn());
    }
    IEnumerator treeSpawn() {
        yield return new WaitForSeconds(Random.Range(10, 20));
        GameObject newTree = Instantiate(tree, transform.position, transform.rotation);

        left_right = Mathf.RoundToInt(Random.Range(0.0f, 1.0f));
        if(left_right == 0) {
            newTree.transform.position = new Vector2(Random.Range(-1.5f, -3), transform.position.y);
            newTree.transform.localEulerAngles = new Vector2(0, 0);
        } else if(left_right == 1) {
            newTree.transform.position = new Vector2(Random.Range(1.5f, 3), transform.position.y);
            newTree.transform.localEulerAngles = new Vector2(0, 180);
        }

        StartCoroutine(treeSpawn());
    }
}
