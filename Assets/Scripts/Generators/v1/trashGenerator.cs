﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trashGenerator : MonoBehaviour {
    public GameObject[] trashToSpawn;
    void Start() {
        //InvokeRepeating("trashSpawn", Random.Range(3, 10), Random.Range(1, 5));
        StartCoroutine(trashSpawn());
    }
    IEnumerator trashSpawn() {
        yield return new WaitForSeconds(Random.Range(2, 5));
        GameObject newTrash = Instantiate(trashToSpawn[Random.Range(0, trashToSpawn.Length)], transform.position, transform.rotation);
    
        newTrash.transform.position = new Vector2(Random.Range(-1.8f, 1.8f), transform.position.y);
        //newTrash.transform.localEulerAngles = new Vector3(0, 0, Random.Range(0, 360));
        
        StartCoroutine(trashSpawn());
    }
}
