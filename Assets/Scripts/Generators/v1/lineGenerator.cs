﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lineGenerator : MonoBehaviour {
    public GameObject line;
    public float delay;
    void Start() {
        InvokeRepeating("Spawn", 0, delay);
    }
    public void Spawn() {
        Instantiate(line, transform.position, transform.rotation);
    }
}
