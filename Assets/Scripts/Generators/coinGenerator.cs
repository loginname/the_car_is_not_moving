﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coinGenerator : MonoBehaviour {
    public GameObject coin;
    void Start() {
        //InvokeRepeating("coinSpawn", Random.Range(5, 10), Random.Range(5, 10));
        StartCoroutine(coinSpawn());
    }
    IEnumerator coinSpawn() {
        yield return new WaitForSeconds(Random.Range(5, 10));
        GameObject newTrash = Instantiate(coin, transform.position, transform.rotation);
    
        newTrash.transform.position = new Vector2(Random.Range(-1.8f, 1.8f), transform.position.y);
    
        StartCoroutine(coinSpawn());
    }
}
