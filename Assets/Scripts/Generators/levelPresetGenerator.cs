﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelPresetGenerator : MonoBehaviour {
    public Transform Camera;
    public presetLevel[] presetsPrefabs; // all chunk can spawn
    private List<presetLevel> spawnedPresets = new List<presetLevel>(); // list of current spawned chunks
    void Start() {
        spawnedPresets.Add(Instantiate(presetsPrefabs[Random.Range(0, presetsPrefabs.Length)])); // add first chunk in list
    }
    void Update() {
        if(Camera.position.y > spawnedPresets[spawnedPresets.Count-1].End.position.y - 6.3) { // check for spawn
            presetSpawn();
        }
    }
    void presetSpawn() {
        presetLevel newPreset = Instantiate(presetsPrefabs[Random.Range(0, presetsPrefabs.Length)]);
        newPreset.transform.position = spawnedPresets[spawnedPresets.Count-1].End.position - newPreset.Begin.localPosition;
        spawnedPresets.Add(newPreset);

        if(spawnedPresets.Count >= 3) {
            Destroy(spawnedPresets[0].gameObject);
            spawnedPresets.RemoveAt(0);
        }
    }
}
