﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objController : MonoBehaviour {
    public bool byStay = false;
    public bool byTouch = false;
    void Update() {
        if(gameObject.CompareTag("coin") && transform.position.y <= Camera.main.transform.position.y - 8) // destroy if go out from screen
            Destroy(gameObject);
    }
    void OnTriggerEnter2D(Collider2D other) {
        if(byStay == true)
            if(gameObject.CompareTag("enemy") && other.CompareTag("coin"))
                Destroy(gameObject);

        if(byTouch == true) {
            if(gameObject.CompareTag("coin") && other.CompareTag("car")) {
                currentGameSituation.currentCoins++;
                Destroy(gameObject);
            } else if(gameObject.CompareTag("enemy") && other.CompareTag("car")) {
                currentGameSituation.gameOver = true;
            }
        }
    }
}
