﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bgGenerator : MonoBehaviour {
    public Transform Camera;
    public backGround bgSprite; // sprite of endless bg
    public static int countOrderInLayer = 0;
    public GameObject firstBG;
    private List<backGround> spawnedBG = new List<backGround>(); // list of current bgs
    void Start() {
        spawnedBG.Add(Instantiate(bgSprite));
    }
    void Update() {
        if(Camera.position.y > spawnedBG[spawnedBG.Count-1].End.position.y - 7) { // check for spawn
            SpawnBG();
        }
    }
    void SpawnBG() {
        countOrderInLayer--;

        backGround newBG = Instantiate(bgSprite);
        newBG.transform.position = spawnedBG[spawnedBG.Count-1].End.position - newBG.Begin.localPosition;
        spawnedBG.Add(newBG);

        if(spawnedBG.Count > 3) {
            Destroy(spawnedBG[0].gameObject);
            Destroy(firstBG);
            spawnedBG.RemoveAt(0);
        }
    }
}
