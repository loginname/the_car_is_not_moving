﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class uiController : MonoBehaviour {
    public GameObject gameUI;
    public GameObject pauseUI;
    public GameObject gameOverUI;
    public GameObject mainMenu;
    public GameObject settingsMenu;
    public GameObject shopMenu;
    public GameObject infoMenu;
    public GameObject onVolumeButton;
    public GameObject offVolumeButton;
    void Start() {
        if(currentGameSituation.firstLaunch == false && currentGameSituation.inGame == false) {
            shopMenu.SetActive(true);
            mainMenu.SetActive(false);
        }

        currentGameSituation.firstLaunch = false; // game already launched
    }
    void Update() {
        if(currentGameSituation.gameOver == true && currentGameSituation.transition == false) {
            currentGameSituation.transition = true;
            gameOver();
        }
    }
    public void toShop() {
        mainMenu.SetActive(false);
        shopMenu.SetActive(true);
    }
        public void nextCar() {
            
        }
        public void previousCar() {

        }
        public void Play() {
            currentGameSituation.loadSceneNumb(1);
        }
            public void toPause() {
                Time.timeScale = 0;

                gameUI.SetActive(false);
                pauseUI.SetActive(true);
            }
                public void toMainMenu() {
                    currentGameSituation.loadSceneNumb(0);
                    // toShop();
                }
            public void fromPause() {
                Time.timeScale = 1;

                pauseUI.SetActive(false);
                gameUI.SetActive(true);
            }
            void gameOver() {
                Time.timeScale = 0;

                if(currentGameSituation.currentCoins > currentGameSituation.bestCoins)
                    currentGameSituation.bestCoins = currentGameSituation.currentCoins;

                currentGameSituation.totalCoins += currentGameSituation.currentCoins;

                gameUI.SetActive(false);
                gameOverUI.SetActive(true);
            }
    public void fromShop() {
        shopMenu.SetActive(false);
        mainMenu.SetActive(true);
    }
/////////////////////////////////////////////////////////////////////////////////////
    public void toSettings() {
        mainMenu.SetActive(false);
        settingsMenu.SetActive(true);
    }
        public void offVolume() {
            AudioListener.volume = 0;

            onVolumeButton.SetActive(false);
            offVolumeButton.SetActive(true);
        }
        public void onVolume() {
            AudioListener.volume = 1;

            offVolumeButton.SetActive(false);
            onVolumeButton.SetActive(true);
        }
        public void toInfo() {
            settingsMenu.SetActive(false);
            infoMenu.SetActive(true);
        }
        public void fromInfo() {
            infoMenu.SetActive(false);
            settingsMenu.SetActive(true);
        }
    public void fromSettings() {
        settingsMenu.SetActive(false);
        mainMenu.SetActive(true);
    }
    public void Exit() {
        Application.Quit();
    }
}
