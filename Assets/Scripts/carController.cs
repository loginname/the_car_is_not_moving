﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carController : MonoBehaviour {
    //private Rigidbody2D rigidBody;
    public Transform rotationCar; // start box'es rotation
    public float sideSpeed = 10;
    public bool isLeft = false;
    public bool isRight = false;
    public bool isDown = false;
    
    private float minSpeed = 2.0f;
    private float maxSpeed = 3.5f;
    private float rateOfChange = 0.5f;
    public static float currentSpeed = 2.0f;
    void Start() {
        //rigidBody = GetComponent<Rigidbody2D>();
    }
    void Update() {
        // if(Input.GetKey(KeyCode.A) && transform.position.x > -1.9) { // Left
        //     transform.Translate(Vector2.left * Time.deltaTime * sideSpeed, Space.World);

        //     if(!(transform.localEulerAngles.z < 20 && transform.localEulerAngles.z > 10)) // if left loon isnt at the edge
        //         transform.Rotate(0, 0, Time.deltaTime * 100); // rotate box to left side
        // } else if(Input.GetKey(KeyCode.D) && transform.position.x < 1.9) { // Right
        //     transform.Translate(Vector2.right * Time.deltaTime * sideSpeed, Space.World);
            
        //     if(!(transform.localEulerAngles.z > 340 && transform.localEulerAngles.z < 350)) // if right loon isnt at the edge
        //         transform.Rotate(0, 0, -Time.deltaTime * 100); // rotate box to right side
        // } else { // return to start position
        //     transform.rotation = Quaternion.Slerp(transform.rotation, rotationCar.rotation, Time.deltaTime * 10);
        // }
        currentSpeed = Mathf.Lerp(currentSpeed, (isDown ? minSpeed : maxSpeed), rateOfChange * Time.deltaTime); // if press button then go to minSpeed, else go to maxSpeed
        //rigidBody.velocity = transform.up * m_speed;
        transform.Translate(Vector2.up * Time.deltaTime * currentSpeed);
        
        if(isLeft == true && isRight == false && transform.position.x > -1.9) { // Left
            transform.Translate(Vector2.left * Time.deltaTime * sideSpeed, Space.World);

            if(!(transform.localEulerAngles.z < 20 && transform.localEulerAngles.z > 10)) // if left loon isnt at the edge
                transform.Rotate(0, 0, Time.deltaTime * 100); // rotate box to left side
        } else if(isRight == true && isLeft == false && transform.position.x < 1.9) { // Right
            transform.Translate(Vector2.right * Time.deltaTime * sideSpeed, Space.World);
            
            if(!(transform.localEulerAngles.z > 340 && transform.localEulerAngles.z < 350)) // if right loon isnt at the edge
                transform.Rotate(0, 0, -Time.deltaTime * 100); // rotate box to right side
        } else { // return to start position
            transform.rotation = Quaternion.Slerp(transform.rotation, rotationCar.rotation, Time.deltaTime * 10);
        }
    }
    public void moveLeft(bool isOn) {
        isLeft = isOn;
    }
    public void moveRight(bool isOn) {
        isRight = isOn;
    }
    public void moveDown(bool isOn) {
        isDown = isOn;
    }
}
