﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lineGenerator : MonoBehaviour {
    public int idleSpeed = 2;
    public GameObject line;
    public float delay;
    private int timeBeforeDelay = 0;
    void Start() {
        InvokeRepeating("Spawn", timeBeforeDelay, delay);
        //Instantiate(line, new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
    }
    void Update() {
        if(currentGameSituation.gameOver == false) {
            transform.Translate(Vector2.up * Time.deltaTime * idleSpeed); // move
        }

        // Invoke("Spawn", delay);
    }
    public void Spawn() {
        Instantiate(line, transform.position, transform.rotation);
    }
}
